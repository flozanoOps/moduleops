<?php

namespace Drupal\opsomai\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Config;


use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\editor\Ajax\EditorDialogSave;



use Drupal\opsomai\OpsomaiService\OpsomaiService;

use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;



/**
 * Configure opsomai settings for this site.
 */
class CKEditorDialog extends ConfigFormBase {



protected $opsomaiService;

/**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'opsomai.settings';


  public function __construct(OpsomaiService $OpsomaiService){
    $this->opsomaiService = $OpsomaiService;
  }


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('opsomai.servicemedia'));
  }


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'opsomai_c_k_editor_dialog';
  }


  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'opsomai.settings',
    ];
  }


  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

      $config = $this->config('opsomai.settings');
      $xml = $this->opsomaiService->getAPI();
      $elementsXML = $this->opsomaiService->getElemXml($xml);

    // Ensure relevant dialog libraries are attached.
    $form['#attached']['library'][] = 'editor/drupal.editor.dialog';
    $form['#attached']['library'][] = 'opsomai/ckeditor';
    $form['#attached']['library'][] = drupal_get_path('module', 'opsomai').'/css/opsomai/opsomaiStyle.css';
    

    $wrapper_id = 'qbank-ckeditor-wrapper' . rand();
    $form['#attached']['drupalSettings']['qbank_dam']['modulePath'] = drupal_get_path('module', 'opsomai');
    $form['#attached']['drupalSettings']['qbank_dam']['html_id'] = $wrapper_id;


    $form['#prefix'] = '<div class="clas_for_page">';
    $form['#suffix'] = '</div>';

     $form['group1'] = array(
      '#type' => 'fieldset',
      '#title' => t('Medias'),
      '#collapsible' => False,
      '#collapsed' => False,
    );

     foreach ($elementsXML as $key => $value) {


          
         $settings['width']= '200px';  //$value['width'] ;
         $settings['height']='200px'; //$value['height'] ;
         $settings['src'] = $value['src'] ;

      $form['group1']['medias'][$key]['#attributes'] = array('class' => array('clas_test'),);
      $form['group1']['medias'][$key]['prefix'] = array('#prefix' => '<div clas="find_class_boostraap"');
      $form['group1']['medias'][$key]['iframe'] = $this->opsomaiService->renderEmbedCode("", $settings);
      $form['group1']['medias'][$key]['checkbox'] = ['#type' => 'checkbox',
        '#title' => $value['reference'],'#default_value' =>false,'#return_value' => "checked",'#attributes' => ['class'=>['ipt_'.$value['reference']]],];
      $form['group1']['medias'][$key]['suffix'] = array('#suffix' => '</div>',);
  }


    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('add Medias'),
      '#ajax' => [
          'callback' => '::nam_function',
          ],
    );


return parent::buildForm($form, $form_state);
}

public function nam_function(array &$form, FormStateInterface $form_state){
}

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
      parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc} Inteneraire du formulare
   */
  //  enregistrer les valeurs du formulaire.
  public function submitForm(array &$form, FormStateInterface $form_state) {

        /*
        $response = new Response();
        $response->setContent('<?xml> My flux </xml>');
        return $response;
        */
      $config = $this->config('opsomai.opsconfig');
      $response = new AjaxResponse();
      $values = $form_state->getValues();

      $tab = array () ;
      $i = 0;

/*
    while (TRUE) {
      if ($values['checkbox'][$i]) {
           $tab[$i]= $values['checkbox'][$i] ; //take champs ;
       }else{
        break;
       }
        $i ++;
   }
*/

     // $media = $this->opsomaiService->download($form_state->getValues('url'), $form_state->getValues('id')) ;
      //$response->addCommand(new EditorDialogSave( $form_state->getValues()));
      //$response->addCommand(new CloseModalDialogCommand( $form_state->getValues()));

    return $response;
  }

}
