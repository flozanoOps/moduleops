<?php

namespace Drupal\opsomai\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;


use Drupal\opsomai\OpsomaiService\OpsomaiService;  // instance api


use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Configure opsomai settings for this site.
 */
class SettingsForm extends ConfigFormBase {


protected $QAPIOPS;

/**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'opsomai.settings';


  public function __construct(OpsomaiService $OpsomaiServiceApi){
    $this->QAPIOPS = $OpsomaiServiceApi;
  }


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('opsomai.servicemedia'));
  }



  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'opsomai_settings';
  }


 /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }



  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

  $config = $this->config(static::SETTINGS);
    // https://www.drupal.org/docs/8/api/configuration-api/working-with-configuration-forms

    // add code test
    $form['group1'] = array(
      '#type' => 'fieldset',
      '#title' => t('Recherche'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );


    $form['group1']['api_url'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Api Url'),
      '#default_value' => '',
      '#size' => '30',
      '#maxlength' => 128,
      '#required' => TRUE,
    );



    $form['group1']['api_key'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Api key'),
      '#default_value' => '',
      '#size' => '30',
      '#maxlength' => 128,
      '#required' => TRUE,
    );

    $form['group3']['btn_add_mapping2'] = [
      '#type' => 'button',
      '#value' => t('Submit'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

  if (!$this->QAPI->checkConfiguration($form_state->getValue('api_url'), $form_state->getValue('api_key')) ) {
      $form_state->setErrorByName('missing Opsomai_url', $this->t('Unable to connect to Opsomai DAM API, please check your configuration'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    //Remplir fichier setting
     $this->config('opsomai.settings')
      ->set('api_url', $form_state->getValue('api_url'))
      ->set('api_key', $form_state->getValue('api_key'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
