<?php
namespace Drupal\opsomai\OpsomaiServiceApi;

use Exception;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;


class OpsomaiServiceApi implements OpsomaiServiceApiInterface {


    private  $urlApi = "http://ch-ops-dev/int/minagri/www/service.php?urlaction=recherche";
    private  $api_key = "E2D6139Z325a49Eh87GL8396";


   /*****
   Constructeur
   ****/
    public function __construct(){
    }

 /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    $container->register('opsomai.serviceApi', 'Drupal\opsomai\OpsomaiServiceApi\OpsomaiServiceApi')
      ->addTag('event_subscriber')
      ->addArgument(new Reference('entity_type.manager'));
  }

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $modules = $container->getParameter('container.modules');
    if (isset($modules['dblog'])) {
      // Override default DB logger to exclude some unwanted log messages.
      $container->getDefinition('logger.dblog')
        ->setClass('Drupal\opsomai\Logger\OpsomaiLog');
    }
  }

   public function getCheckArg($url,$api_key){
      if($this->urlApi !== $url || $this->api_key !== $api_key ){
        return false;
      }
      return  true ;
   }


   public function getParamet(){
    $tab['url'] = $this->urlApi;
    $tab['api_key'] = $this->api_key;
    return $tab;
   }
}
