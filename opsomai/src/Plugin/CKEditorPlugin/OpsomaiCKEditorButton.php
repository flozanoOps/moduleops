<?php

namespace Drupal\opsomai\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\editor\Entity\Editor;

use Drupal\opsomai\OpsomaiService\OpsomaiService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the "opsomai" plugin.
 *
 * NOTE: The plugin ID ('id' key) corresponds to the CKEditor plugin name.
 * It is the first argument of the CKEDITOR.plugins.add() function in the
 * plugin.js file.
 *
 * @CKEditorPlugin(
 *   id = "opsomai",
 *   label = @Translation("opsomai damckeditor button")
 * )
 */
class OpsomaiCKEditorButton extends CKEditorPluginBase implements ContainerFactoryPluginInterface {

  protected $QAPI;

  /**
   * Constructs a Drupal\entity_embed\Plugin\CKEditorPlugin\DrupalEntity object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\Query\QueryInterface $embed_button_query
   *   The entity query object for embed button.
   */


  public function __construct(array $configuration, $plugin_id, $plugin_definition, OpsomaiService $opsomai_api) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->QAPI = $opsomai_api;
  }


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('opsomai.servicemedia')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function getButtons() {

    return [
      'example' => [
        'label' => $this->t('Opsomai'),
        'image' =>  drupal_get_path('module', 'opsomai') . '/js/plugins/opsomai/images/Opsomai.png',
      ],
    ];
  }


  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return drupal_get_path('module', 'opsomai') . '/js/plugins/opsomai/plugin.js';
  }


  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [
      'opsomai' => [
        'url' =>   $this->QAPI->getApiUrl(),
        'modulePath' =>  drupal_get_path('module', 'opsomai'),
        'html_id' =>  'qbank-ckeditor-wrapper' . rand(),
        ]
    ];
  }


  /**
   * {@inheritdoc}
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [
      'opsomai/connector',
    ];
  }




}
