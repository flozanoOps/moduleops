<?php

namespace Drupal\opsomai\Plugin\EntityBrowser\Widget;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\entity_browser\WidgetBase;
use Drupal\entity_browser\WidgetValidationManager;

use Drupal\opsomai\OpsomaiService\OpsomaiService; //  opsomai service 
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Integration with QBank DAM library.
 *
 * @EntityBrowserWidget(
 *   id = "qbank_dam",
 *   label = @Translation("QBank DAM"),
 *   description = @Translation("Integrates with QBank DAM library")
 * )
 */
class opsomai extends WidgetBase {

    /**
     * Current user service.
     *
     * @var \Drupal\Core\Session\AccountProxyInterface
     */
    protected $currentUser;
    protected $QAPI;

    /**
     * {@inheritdoc}
     */
    public function defaultConfiguration() {
        return parent::defaultConfiguration();
    }

    /**
     * Constructs a new View object.
     *
     * @param array $configuration
     *   A configuration array containing information about the plugin instance.
     * @param string $plugin_id
     *   The plugin_id for the plugin instance.
     * @param mixed $plugin_definition
     *   The plugin implementation definition.
     * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
     *   Event dispatcher service.
     * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
     *   The entity type manager.
     * @param \Drupal\entity_browser\WidgetValidationManager $validation_manager
     *   The Widget Validation Manager service.
     */


    
    public function __construct(array $configuration, $plugin_id, $plugin_definition, EventDispatcherInterface $event_dispatcher, EntityTypeManagerInterface $entity_type_manager, WidgetValidationManager $validation_manager, OpsomaiService $qbank_api, AccountProxyInterface $current_user) {
        parent::__construct($configuration, $plugin_id, $plugin_definition, $event_dispatcher, $entity_type_manager, $validation_manager);
        $this->QAPI = $qbank_api;
        $this->currentUser = $current_user;

    }




-

    

}
