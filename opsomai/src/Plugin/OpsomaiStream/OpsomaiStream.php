<?php
namespace Drupal\opsomai\Plugin\OpsomaiStream;


use Drupal\video\ProviderPluginBase;
use Drupal\opsomai\OpsomaiService\OpsomaiService;


//adaptation de plugin iframe pour voir videos opsomai  dans l'iframe 
class OpsomaiStream extends ProviderPluginBase {
  



   protected $QAPI ; 
   protected static $base_url = 'http://www.youtube.com/watch';




    public function __construct( OpsomaiService $opsomai_api) {
            $this->QAPI = $opsomai_api;
    }



  /**
   * {@inheritdoc}
   */
  public function renderEmbedCode($settings) {
    $file = $this->getVideoFile();
    $data = $this->getVideoMetadata();



  
    return [
      '#type' => 'html_tag',
      '#tag' => 'iframe',
      '#attributes' => [
        'width' => $settings['width'],
        'height' => $settings['height'],
        'frameborder' => '0',
        'allowfullscreen' => 'allowfullscreen',
        'src' => sprintf('https://www.youtube.com/embed/%s?autoplay=%d&start=%d', $settings['id'], $settings['autoplay'], NULL),
      ],
    ];


    
  }
  

  
  /**
   * {@inheritdoc}
   */
  public function getRemoteThumbnailUrl() {
    $data = $this->getVideoMetadata();
    return 'http://img.youtube.com/vi/' . $data['id'] . "/maxresdefault.jpg";
  }


  
}
