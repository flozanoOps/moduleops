<?php

namespace Drupal\opsomai\OpsomaiService;
use DOMDocument;

use Drupal\opsomai\OpsomaiServiceApi\OpsomaiServiceApi;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

use Drupal\Core\Mail\MailManager;
use Drupal\Core\Render\RendererInterface;

use Exception;
use GuzzleHttp\Client;

//plugin youtubeStrim / plugin video
use Drupal\Core\StreamWrapper\ReadOnlyStream;
use Drupal\Core\StreamWrapper\StreamWrapperInterface;
use Drupal\video\StreamWrapper\VideoRemoteStreamWrapper;



class OpsomaiService extends VideoRemoteStreamWrapper implements OpsomaiServiceInterface{

    use StringTranslationTrait;

    protected $QAPI;


    //constructeur opsomai
    public function __construct(){
            $this->QAPI = NULL; //api opsomai
    }

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    $container->register('opsomai.servicemedia', 'Drupal\opsomai\OpsomaiService\OpsomaiService')
      ->addTag('event_subscriber')
      ->addArgument(new Reference('entity_type.manager'));
  }


  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $modules = $container->getParameter('container.modules');
    if (isset($modules['dblog'])) {
      // Override default DB logger to exclude some unwanted log messages.
      $container->getDefinition('logger.dblog')
        ->setClass('Drupal\opsomai\Logger\OpsomaiLog');
    }
  }


    /**
     * @param $url
     * @param $media_id
     * @return \Drupal\file\FileInterface|false|null
     */
    public function download($url, $media_id)
    {
        $file = NULL;

        $client = new Client();
        $response = $client->get($url);
        if ($response->getStatusCode() == 200) {
        $filenameOriginal = str_replace('"', '', explode('filename=', $response->getHeader('content-disposition')[0])[1]);
            $filenameOriginal = "" ;
            $filenameParts = explode('.', $filenameOriginal);
            $ext = array_pop($filenameParts);
            $filenameOnly = implode('.', $filenameParts);
            $filename = $filenameOnly . date('YmdHis') . '.' . $ext;
            if ($filename) {
                $file_data = "";  //$response->getBody()->getContents();
                $directory = 'public://opsomai/'; // create and definition in setting.php
                file_prepare_directory($directory, FILE_CREATE_DIRECTORY);
                $file = file_save_data($file_data, $directory . $filename, FILE_EXISTS_REPLACE);
            }
      }

        return $file;
    }



    // check just connection api
    public function checkConfiguration($url , $api_key){

      try{
            $test = new OpsomaiServiceApi();

            $test->getCheckArg($url,$api_key);

        } catch (Exception $e) {

            return NULL;
        }

        return TRUE;
    }

 /**
     * @return Opsomai|null
     */
    public function getAPI()
    {

            if ($this->QAPI == NULL){

                $this->QAPI = new OpsomaiServiceApi();
                $conf =  $this->QAPI->getParamet();
            }

            $doc = new DOMDocument();
            $client = new Client();

        try {

                $response = $client->request('POST', $conf['url'] ,
                  ['form_params' => ['api_key'=>  $conf['api_key']],'verify' => false]);
              if( $response->getStatusCode() === 200){

                      // objet
                     return simplexml_load_string(  $response->getBody()->getContents() ) ;
                      //string  xml

                   // return $response->getBody()->getContents() ;  string

                    //return $doc->saveXML($doc->createTextNode( $response->getBody()->getContents())); // xml

              }else{
                  return $this->QAPI;
              }

        } catch (\LogicException $e) {
            \Drupal::logger('opsomai')->error($e->getMessage());
        } catch (\InvalidArgumentException $e) {
            \Drupal::logger('opsomai')->error($e->getMessage());
        } catch (\Exception $e) {
            \Drupal::logger('opsomai')->error($e->getMessage());
        }
    }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return t('Opsomai');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return t('Video served by the Opsomai services.');
  }

  public function renderEmbedCode( $src = "" , $settings = array()) {

      if(!empty($settings)){
            //for video
            if( isset($settings['tag']) and $settings['tag'] === 'iframe'){
                            return [
                              '#type' => 'html_tag',
                              '#tag' => 'iframe',
                              '#attributes' => [
                                'width' => $settings['width']."px",
                                'height' => $settings['height']."px",
                                'frameborder' => '0',
                                'allowfullscreen' => 'allowfullscreen',
                                'src' => sprintf('https://www.youtube.com/embed/%s?autoplay=%d&start=%d', $settings['id'], $settings['autoplay'], NULL),
                              ],
                            ];
             }else{

                      return [
                              '#type' => 'html_tag',
                              '#tag' => 'img',
                              '#attributes' => [
                              'width' => $settings['width']."px",
                              'height' => $settings['height']."px",
                              'src' =>$settings['src'] ,],
                        ];
             }

      }



}

  // function NULL ,  best version
  public function getElemXml($xml){

       try {

          $elements = array(); 
          if(!empty($xml)){  
                  foreach ($xml->data->rows->row as $character) {
                     $elements["'".$character->numero."'"][ "src"]=strval($character->vignette);
                     $elements["'".$character->numero."'"]["reference"]=strval($character->reference);
                     $elements["'".$character->numero."'"]["width"]=strval( $character->width);
                     $elements["'".$character->numero."'"]["height"]=strval( $character->height);
                  }
           }
      }catch (\LogicException $e) {
            \Drupal::logger('opsomai')->error($e->getMessage());
        } catch (\InvalidArgumentException $e) {
            \Drupal::logger('opsomai')->error($e->getMessage());
        } catch (\Exception $e) {
            \Drupal::logger('opsomai')->error($e->getMessage());
        }



        return $elements;

}



  public function sendMail($parament){

    $mail_message = [
      '#type' => $parament['theme'],
      '#theme' => $parament['my_module_xml_mail'],
      '#name' => $parament['Name'],
      '#phone' => $parament['Phone'],
      '#email' => $parament['E-mail'],
    ];

$rendered = \Drupal::service('renderer')->render($mail_message);

\Drupal::service('plugin.manager.mail')->mail('opsomai', 'xml_mail', 'test@example.com', 'en', [
  'message' => str_replace(['<', '>'], ['&#60;', '&#62;'], $rendered->__toString()),
]);

}

public function getApiUrl(){

  return  [] ;

}



}
